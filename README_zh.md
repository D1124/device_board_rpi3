# device_board_iscas

## 简介

#### ISCAS简介

中科院软件所（ISCAS）智能软件研究中心（ISRC）的使命是以智能驱动软件发展，以软件支撑智能创新。面向智能计算发展趋势，瞄准高性能、高安全、低功耗、低延时等需求，研究智能基础理论与模型、软件新结构与编译构造方法、内核和运行时环境等，打造适配通用处理器、智能芯片和开放指令集的操作系统和编译工具链，建设开源软件可靠供应链和安全漏洞平台，研发智能无人系统仿真测试环境，支撑智能计算生态和重要行业应用。

#### 开发板简介

- [树莓派3B]

树莓派是一款基于ARM的微型电脑主板，以SD/MicroSD卡为内存硬盘，卡片主板周围有1/2/4个USB接口和一个10/100 以太网接口（A型没有网口），可连接键盘、鼠标和网线，同时拥有视频模拟信号的电视输出接口和HDMI高清视频输出接口。


## 目录

```
device/board/iscas
├── rpi3                                # 树莓派3b开发板
└── ...
```

## 使用说明

树莓派3b参考：
- [树莓派3b](https://gitee.com/openharmony/device_board_iscas/rpi3/README_zh.md)


## 相关仓

* [vendor\iscas](https://gitee.com/openharmony/vendor_iscas)
* [device\soc\broadcom](https://gitee.com/openharmony/device_soc_broadcom)